dockerfile-xpra
===============

Simple example of using xpra inside a docker container. This example uses Xdummy with xpra which is required to use opengl. See the branch no-gl for an example that uses the standard Xvfb with xpra.

Usage
-----

Make sure you are in the same directory as the Dockerfile

    cd dockerfile-xpra

Build the image and tag it xpra. The -rm options cleans the cache after the final image ha been created.

    docker build -t xpra -rm .

Run it, mapping the 22 port in the container to 1022 on the host.

    docker run -i -t -p 10000:10000 xpra-glxgears

In a separate terminal connect to xpra running in the container. The password is changeme.

    xpra attach tcp:localhost:10000

If you return to the first terminal and hit any key then glxgears will launch on the host. If you already hit any key then glxgears will open on the host as soon as you attached to xpra. I only added the extra need to press a key before launching glxgears because I intend to use this example to run applications that may contain intros (eg. Dwarf Fortress).

Host device kernel version and xpra version:
   root@pc:/home/administrator/programming/dockerfile-xpra# lsb_release -a
	No LSB modules are available.
	Distributor ID:	Ubuntu
	Description:	Ubuntu 16.04.4 LTS
	Release:	16.04
	Codename:	xenial

   root@pc:/home/administrator/programming/dockerfile-xpra# xpra version

	Warning: running as root
	Warning: the python netifaces package is missing
	2.2.5-r18663


Todo
----

* The line that prints start.sh is too long, it would be good to extract common functionality into separate utilities that can be fetched from gitub. I do not include it as a separate file since I want the Dockerfile to be as self contained as possible.
